/*
 * Nathan Cerice, CS201, Project 1
 */
#include <stdio.h>
#include "queue.h"
#include "stack.h"
#ifndef HEAP_H_
#define HEAP_H_
typedef struct Heap {
    struct Treenode *root;
    struct Stack *heapStack;
    struct Queue *heapQueue;
    struct Stack *lastLeafStack;
    char *type;
    int count;
} Heap;

Heap *createHeap(char *type);
void heapAdd(Heap *h, int value);
void buildHeap(Heap *h);
void heapify(Heap *h, Treenode *n);
int extractMax(Heap *h);
void swapNodeValues(Treenode *node1, Treenode *node2);
#endif
