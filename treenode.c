/*
 * Nathan Cerice, CS201, Project 1
 */
#include <stdio.h>
#include <stdlib.h>
#include "treenode.h"

Treenode *newTreeNode(int value) {
    Treenode *p = malloc(sizeof(Treenode));
    p->value = value;
    p->parent = NULL;
    p->leftChild = NULL;
    p->rightChild = NULL;
    return p;
}


