/*
 * Nathan Cerice, CS201, Project 1
 */
#include <stdlib.h>
#include <stdio.h>
#include "stack.h"

//constructor
Stack *createStack() {
    Stack *s = malloc(sizeof(Stack));
    s->front = NULL;
    s->count = 0;
    return s;
}

//functions
void push(Stack *s, Treenode *n) {
    QueueStackNode *temp = newNode(n);
    if (s->front == NULL) {
        s->front = temp;
        ++s->count;
        return;
    }
    temp->next = s->front;
    s->front = temp;
    ++s->count;
}

QueueStackNode *pop(Stack *s) {
    if (s->front == NULL) {
        printf("There is nothing in the stack to pop!\n");
        return NULL;
    }
    QueueStackNode *temp = s->front;
    s->front = s->front->next;
    --s->count;
    return temp;
}
