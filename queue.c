/*
 * Nathan Cerice, CS201, Project 1
 */
#include <stdlib.h>
#include <stdio.h>
#include "queue.h"

//constructor
Queue *createQueue() {
    Queue *q = malloc(sizeof(Queue));
    q->front = q->rear = NULL;
    q->count = 0;
    return q;
}

//functions
void enQueue(Queue *q, Treenode *n) {
    QueueStackNode *temp = newNode(n);
    if (q->rear == NULL){
        q->front = q->rear = temp;
        ++q->count;
        return;
    }
    q->rear->next = temp;
    q->rear = temp;
    ++q->count;
}

QueueStackNode *deQueue(Queue *q) {
    if (q->front == NULL){
        printf("There is nothing in the queue to dequeue!\n");
        return NULL;
    }
    QueueStackNode *temp = q->front;
    q->front = q->front->next;

    if (q->front == NULL) {
        q->rear = NULL;
    }
    --q->count;
    return temp;
}

QueueStackNode *peekQueue(Queue *q) {
    if (q->front != NULL) {
        return q->front;
    }
    printf("There is nothing in the queue to peek!\n");
    return NULL;
}
