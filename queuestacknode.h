/*
 * Nathan Cerice, CS201, Project 1
 */
#include <stdio.h>
#include "treenode.h"
#ifndef QUEUESTACKNODE_H_
#define QUEUESTACKNODE_H_

typedef struct QueueStackNode {
    Treenode *node;
    struct QueueStackNode *next;
} QueueStackNode;

QueueStackNode *newNode(Treenode *n);
#endif
