/*
 * Nathan Cerice, CS201, Project 1
 */
#include <stdio.h>
#ifndef TREENODE_H_
#define TREENODE_H_

typedef struct Treenode {
    int value;
    struct Treenode *parent;
    struct Treenode *leftChild;
    struct Treenode *rightChild;
} Treenode;

Treenode *newTreeNode(int value);
#endif
