/*
 * Nathan Cerice, CS201, Project 1
 */
#include <stdio.h>
#include "queuestacknode.h"
#ifndef QUEUE_H_
#define QUEUE_H_

typedef struct Queue {
    struct QueueStackNode *front; 
    struct QueueStackNode *rear;
    int count;
} Queue;

Queue *createQueue();
void enQueue(Queue *q, Treenode *n);
QueueStackNode *deQueue(Queue *q);
QueueStackNode *peekQueue(Queue *q);
#endif
