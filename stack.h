/*
 * Nathan Cerice, CS201, Project 1
 */
#include <stdio.h>
#include "queuestacknode.h"
#ifndef STACK_H_
#define STACK_H_
typedef struct Stack {
    struct QueueStackNode *front;
    int count;
} Stack;

Stack *createStack();
void push(Stack *s, Treenode *n);
QueueStackNode *pop(Stack *s);
#endif
