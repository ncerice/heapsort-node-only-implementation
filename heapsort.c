/*
 * Nathan Cerice, CS201, Project 1
 * This program takes in a file of integers and displays it to the console in sorted, increasing order (implemented using a node-based heapsort).
 * The user can make the program sort in decreasing order by adding the -d argument before the integer file name.
 * The user can add the -v option to receive information about the running time of the program.
 */
#include <stdlib.h>
#include <stdio.h>
#include "heap.h"

int main(int argc, char **argv) {
    int num = 0;
    int count = 0;
    if (argv[1] && argv[1][1] == 'v') {
        printf("\nCreated by Nathan Cerice\n");
        printf("CS201 Project 1\n");
        printf("Feb 12 2016\n");
        printf("Time Complexity: Theta(nlogn)\n\n");
        printf("Adding all values to the unordered heap takes n time. Turning the unordered heap into an ordered heap takes nlogn time.\n");
        printf("Extracting the min/max, finding the last leaf, replacing the root value, and heapifying the root n times takes nlogn time overall.\n");
        printf("Therefore, the entire program takes (n + nlogn + nlogn) time, which is the same as Theta(nlogn).\n\n");
        printf("n      |   time\n");
        printf("-----------------------\n");
        printf("10     |   0.000136 sec\n");
        printf("100    |   0.000207 sec\n");
        printf("1000   |   0.001641 sec\n");
        printf("10000  |   0.016252 sec\n");
        printf("100k   |   0.218410 sec\n");
        printf("1m     |   2.893902 sec\n");
        printf("10m    |  37.152761 sec\n");
        printf("Program execution times were testing using <time.h>\n");
        exit(1);
    }
    else if (argv[1] && argv[1][1] == 'd') { //sorts numbers in decreasing order (creates max heap)
        FILE *fp = fopen(argv[2], "r");
        if (fp == 0) {
            fprintf(stderr, "The file data could not be opened for reading\n");
            exit(1);
        }
        Heap *h = createHeap("max");
        while (fscanf(fp, "%d", &num) == 1) {
            heapAdd(h, num);
            ++count;
        }
        fclose(fp);
        buildHeap(h);
        for (int i = 0; i < count; ++i) {
            printf("%d ", extractMax(h));
        }
    }
    else if (argc == 2) { //sorts numbers in increasing order (creates min heap)
        FILE *fp = fopen(argv[1], "r");
        if (fp == 0) {
            fprintf(stderr, "The file data could not be opened for reading\n");
            exit(1);
        }
        Heap *h = createHeap("min");
        while (fscanf(fp, "%d", &num) == 1) {
            heapAdd(h, num);
            ++count;
        }
        fclose(fp);
        buildHeap(h);
        for (int i = 0; i < count; ++i) {
            printf("%d ", extractMax(h));
        }
    }
    else {//invalid arguments
        printf("Invalid arguments. You must run the program in one of the following ways:\n");
        printf("\t heapsort file\n");
        printf("\t heapsort -d file (for sorting in descending order)\n");
        printf("\t heapsort -v (to obtain information about the time complexity of the program)\n");
        printf("Exiting...\n");
        exit(1);
    }
    printf("\n");
    return 0;
}
