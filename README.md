*Nathan Cerice

*University of Alabama

*nacerice@crimson.ua.edu

*CS201


1. This heapsort is implemented only using nodes. No arrays are used, including the stack and queues.
2. The numbers in the input text file can be separated by arbitrary amounts of whitespace.

*Compile the program using 'make', then run the program in one of the following ways:

1. heapsort filename       (sorts in increasing order)

2. heapsort -d filename    (sorts in decreasing order)

3. heapsort -v             (displays general information about the time complexity of the program)