OBJS = heapsort.o heap.o queue.o queuestacknode.o stack.o treenode.o
heapsort: $(OBJS)
	gcc -Wall -g $(OBJS) -o heapsort
heapsort.o: heapsort.c heap.h
	gcc -Wall -g -c heapsort.c
heap.o: heap.c heap.h
	gcc -Wall -g -c heap.c
queue.o: queue.c queue.h
	gcc -Wall -g -c queue.c
queuestacknode.o: queuestacknode.c queuestacknode.h
	gcc -Wall -g -c queuestacknode.c
stack.o: stack.c stack.h
	gcc -Wall -g -c stack.c
treenode.o: treenode.c treenode.h
	gcc -Wall -g -c treenode.c
clean:
	rm -f *.o
