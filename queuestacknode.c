/*
 * Nathan Cerice, CS201, Project 1
 */
#include <stdlib.h>
#include "queuestacknode.h"

QueueStackNode *newNode(Treenode *n) {
    QueueStackNode *t = malloc(sizeof(QueueStackNode));
    t->node = n;
    t->next = NULL;
    return t;
}
