/*
 * Nathan Cerice, CS201, Project 1
 * Heapify function based off code by Vamsi Sangam (theoryofprogramming.com). The function was then modified by Nathan Cerice on 2/5/2016 to fit requirements for project 1.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "heap.h"

//constructor
Heap *createHeap(char *type) {
    Heap *h = malloc(sizeof(Heap));
    h->root = NULL;
    h->heapStack = createStack();
    h->heapQueue = createQueue();
    h->lastLeafStack = createStack();
    h->type = type;
    h->count = 0;
    return h;
}

//functions
//uses the queue to add new nodes to the heap, each in constant time (linear overall)
void heapAdd(Heap *h, int value) {
    Treenode *temp = newTreeNode(value);
    if (h->root == NULL) {
        h->root = temp;
        enQueue(h->heapQueue, temp);
        push(h->heapStack, temp);
        ++h->count;
        return;
    }
    Treenode *nodeCheck = peekQueue(h->heapQueue)->node;
    if (nodeCheck->leftChild == NULL) { //nodeCheck has no children
        peekQueue(h->heapQueue)->node->leftChild = temp;
        temp->parent = peekQueue(h->heapQueue)->node;
    }
    else if (nodeCheck->leftChild != NULL && nodeCheck->rightChild == NULL) {
        peekQueue(h->heapQueue)->node->rightChild = temp; //nodecheck only has left child
        temp->parent = peekQueue(h->heapQueue)->node;
    }
    else {
        deQueue(h->heapQueue); //nodecheck has both children. Dequeues, then checks next node in queue
        peekQueue(h->heapQueue)->node->leftChild = temp;
        temp->parent = peekQueue(h->heapQueue)->node;
    }
    enQueue(h->heapQueue, temp);
    push(h->heapStack, temp);
    push(h->lastLeafStack, temp);//This stack is used for finding the last leaf in heap (for extractTop function)
    ++h->count;
    return;
}

void buildHeap(Heap *h) {
    while (h->heapStack->count > 0) {
        Treenode *currentNode = pop(h->heapStack)->node;
        heapify(h, currentNode);
    }
}

void heapify(Heap *h, Treenode *n) {
    if (strncmp(h->type, "max", 3) == 0) { //IF block is heapify function for max heap (decreasing order sort)
        if (n->leftChild != NULL && n->rightChild != NULL) {
            // both children exist
            if (n->value < n->leftChild->value && n->value < n->rightChild->value) {
                if (n->leftChild->value > n->rightChild->value) {
                    swapNodeValues(n, n->leftChild);
                    heapify(h, n->leftChild);
                } else {
                    swapNodeValues(n, n->rightChild);
                    heapify(h, n->rightChild);
                }
            } else if (n->value < n->leftChild->value && n->value >= n->rightChild->value) {
                swapNodeValues(n, n->leftChild);
                heapify(h, n->leftChild);
            } else if (n->value >= n->leftChild->value && n->value < n->rightChild->value) {
                swapNodeValues(n, n->rightChild);
                heapify(h, n->rightChild);
            }
        } else if (n->rightChild == NULL && n->leftChild != NULL) {
            // Only the leftChild exists
            if (n->leftChild->value > n->value) {
                swapNodeValues(n, n->leftChild);
            }
        }
    }
    else { // ELSE block is heapify function for min heap (increasing order sort)
        if (n->leftChild != NULL && n->rightChild != NULL) {
            // both children exist
            if (n->value > n->leftChild->value && n->value > n->rightChild->value) {
                if (n->leftChild->value < n->rightChild->value) {
                    swapNodeValues(n, n->leftChild);
                    heapify(h, n->leftChild);
                } else{
                    swapNodeValues(n, n->rightChild);
                    heapify(h, n->rightChild);
                }
            } else if (n->value > n->leftChild->value && n->value <= n->rightChild->value) {
                swapNodeValues(n, n->leftChild);
                heapify(h, n->leftChild);
            } else if (n->value <= n->leftChild->value && n->value > n->rightChild->value) {
                swapNodeValues(n, n->rightChild);
                heapify(h, n->rightChild);
            }
        } else if (n->rightChild == NULL && n->leftChild != NULL) {
            // Only the leftChild exists
            if (n->leftChild->value < n->value) {
                swapNodeValues(n, n->leftChild);
            }
        }
    }
}

//pops lastLeafStack to get last leaf in heap, then swaps values with root, prunes leaf, and heapifies root.
//max refers to top of heap (root)
int extractMax(Heap *h) {
    int maxValue = h->root->value;
    if (h->count > 1) {
        Treenode *lastLeaf = pop(h->lastLeafStack)->node;
        h->root->value = lastLeaf->value;
        if (lastLeaf->parent->leftChild != NULL && lastLeaf->parent->rightChild != NULL) {
            lastLeaf->parent->rightChild = NULL;
            lastLeaf->parent = NULL;
            free(lastLeaf);
        }
        else if (lastLeaf->parent->rightChild == NULL) {
            lastLeaf->parent->leftChild = NULL;
            lastLeaf->parent = NULL;
            free(lastLeaf);
        }
        heapify(h, h->root);
    }
    --h->count;
    return maxValue;
}

void swapNodeValues(Treenode *node1, Treenode *node2) {
    int temp = node1->value;
    node1->value = node2->value;
    node2->value = temp;
    return;
}
